# Minecraft Resourcepack Deployment
This repository will help you if you have a gitlab repository with a minecraft resource pack, and you want to create a link to this resource pack for multiple branches.

## Usage
### This repo
In `.gitlab-ci.yml`, change the `REPO=` line to have your own gitlab repo (Developers must have pipeline access to both projects)
### In the other repo
In `.gitlab-ci.yml`, add something like this:
```yaml
release:
  stage: deploy
  only:
    - master
    - dev
  trigger: mrcl1/rpdeploy #Change this to the location of your fork
```
### Getting the links
The links follow the convention: (pages url)/(branch)/(commithash).zip
They're also printed in the CI logs.

## Configuration
You can configure which branches to deploy by changing the `_BRANCHES` variable in the build script.

## PackSquash
This pipeline comes with a binary of [PackSquash](https://packsquash.aylas.org/), please refer to that site for more info, and their source code.
PackSquash is configured with `options.toml` (again, see the site for more details).
