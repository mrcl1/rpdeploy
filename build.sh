#!/bin/bash
PREFIX="${CI_PAGES_URL}"
# REPO=
set -e
_BRANCHES=( "master" "dev" "builder")
if [ ! -z $1 ];then
    _BRANCHES=$1
    echo $_BRANCHES
fi

rm -rf build/

get_latest_commit() {
    cd repo
    git checkout $1 &> /dev/null
    echo $(git rev-parse HEAD)
    cd ..
}
get_url() {
    echo "${PREFIX}/${1}/${2}.zip"
}
make_build() {
    cd repo/
    git checkout $1 &> /dev/null
    local zip_name="$(git rev-parse HEAD).zip"
    ../packsquash ../options.toml
    mv pack.zip $zip_name
    mkdir -p "../build/${1}"
    mv $zip_name "../build/${1}"
    cd ../
}
rm -rf repo ; GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" git clone $REPO repo/
mkdir build/
for t in ${_BRANCHES[@]}; do
    make_build $t
done

mkdir public/
mv -f build/* public/


for t in ${_BRANCHES[@]}; do
   get_url $t $(get_latest_commit $t)
done
